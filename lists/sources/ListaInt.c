#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "../headers/ListaInt.h"

//Insere um elemento na lista. 
//Retorna -1 caso a lista esteja cheia. 
int pushLista(PDado lista, int valor, int fim){
  if (isFull(lista))
    return -1;
  
  lista->topo++;
  if (fim == 1)
	  lista->dado[lista->topo]=valor;
  else {
    int x;
    for(x=lista->topo; x > 0; x--)
      lista->dado[x] = lista->dado[x-1];
    lista->dado[0] = valor;
  }
  
  return 0;
}

//Retira um elemento na lista 
//Retorna -1 caso a lista esteja vazia. 
int popLista(PDado lista, int* valor, int fim){
  if (isEmpty(lista))
    return -1;
  
  if (fim == 1)
    *valor = lista->dado[lista->topo];
  else {
    *valor = lista->dado[0];
    int x;
    for(x=0; x < lista->topo; x++)
      lista->dado[x] = lista->dado[x+1];
  }
  
  lista->topo--;
  return 0;
}

// Lê um conjunto de números inteiros do arquivo "inteiro.txt",
// cria uma lista e os coloca na lista.
void readLista(char arquivo[], PDado l){
  criar(l);
  char numStr[10];
  int valor;
  FILE *arq;
  arq = fopen(arquivo, "r");

  if(arq == NULL)
    printf("Erro, nao foi possivel abrir o arquivo\n");
  else{
//    while (fscanf(arq, "%s", numStr) != EOF){
    while (fscanf(arq, "%d", &valor) != EOF){
     // valor = atoi(numStr);
      printf("Valor lido: %d   ", valor);
      if (isFull(l)){
        printf("lista cheia ... \n");
        break;
      }
      if (pushLista(l, valor, 1) != -1)
        printf("Inserido %d\n", valor);
    }
  }
  fclose(arq);
}
 
