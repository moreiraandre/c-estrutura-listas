#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "../headers/FilaInt.h"

//Insere um elemento na fila. 
//Retorna -1 caso a fila esteja cheia. 
int pushFila(PDado fila, int valor){
  if (isFull(fila))
    return -1;
  fila->topo++;
  fila->dado[fila->topo]=valor;
  return 0;
}

//Retira um elemento na fila 
//Retorna -1 caso a fila esteja vazia. 
int popFila(PDado fila, int* valor){
  if (isEmpty(fila))
    return -1;
    
  *valor = fila->dado[0];
  
  int x;
  for(x=0; x < fila->topo; x++)
    fila->dado[x] = fila->dado[x+1];
  fila->topo--;
  return 0;
}

// Lê um conjunto de números inteiros do arquivo "inteiro.txt",
// cria uma fila e os coloca na fila.
void readFila(char arquivo[], PDado p){
  criar(p);
  char numStr[10];
  int valor;
  FILE *arq;
  arq = fopen(arquivo, "r");

  if(arq == NULL)
    printf("Erro, nao foi possivel abrir o arquivo\n");
  else{
//    while (fscanf(arq, "%s", numStr) != EOF){
    while (fscanf(arq, "%d", &valor) != EOF){
     // valor = atoi(numStr);
      printf("Valor lido: %d   ", valor);
      if (isFull(p)){
        printf("fila cheia ... \n");
        break;
      }
      if (pushFila(p, valor) != -1)
        printf("Inserido %d\n", valor);
    }
  }
  fclose(arq);
}
 
