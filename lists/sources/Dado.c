#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "../headers/Dado.h"

//Atribui valores iniciais para a variável criada
void criar(PDado lista){
    lista->topo=-1;
}

//Verifica se a lista está cheia.
//Retorna: 1 caso a lista esteja cheia; retorna 0 caso contrario.
int isFull(PDado lista){
  return (lista->topo >= MAX-1) ? 1 : 0;
}

//Verifica se a lista está vazia.
//Retorna: 1 caso a lista esteja vazia; retorna 0 caso contrario.
int isEmpty(PDado lista){
  return (lista->topo < 0) ? 1 : 0;
}

