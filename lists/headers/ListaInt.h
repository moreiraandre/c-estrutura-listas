#ifndef ListaInt_xxx_1234

#include "Dado.h"

//Insere um elemento na Lista. 
//Retorna -1 caso a Lista esteja cheia. 
int pushLista(PDado, int, int);

//Retira um elemento na Lista 
//Retorna -1 caso a Lista esteja vazia. 
int popLista(PDado, int*, int);

void readLista(char arquivo[], PDado);

#define ListaInt_xxx_1234

#endif

