All:    Principal.o Dado.o PilhaInt.o FilaInt.o ListaInt.o testePilhaInt.o testeFilaInt.o testeListaInt.o
	gcc Principal.o Dado.o PilhaInt.o FilaInt.o ListaInt.o testePilhaInt.o testeFilaInt.o testeListaInt.o -o saida
	rm -rf *.o

Principal.o:
	gcc -c Principal.c

Dado.o: 
	gcc -c lists/sources/Dado.c

# *** LISTAS ***
PilhaInt.o: 
	gcc -c lists/sources/PilhaInt.c
	
FilaInt.o: 
	gcc -c lists/sources/FilaInt.c
	
ListaInt.o: 
	gcc -c lists/sources/ListaInt.c
# *** TESTES ***
testePilhaInt.o: 
	gcc -c tests/sources/testePilhaInt.c
	
testeFilaInt.o: 
	gcc -c tests/sources/testeFilaInt.c
	
testeListaInt.o: 
	gcc -c tests/sources/testeListaInt.c
# === FIM TESTES ===

clean:
	rm -rf *.o saida

#mrproper: clean
#  rm -rf 
