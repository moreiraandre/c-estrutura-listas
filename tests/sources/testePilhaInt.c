#include <stdio.h>
#include "../../lists/headers/PilhaInt.h"
// Teste de insercao
void testeInserirPilha(PDado pilha){
   int i=0;
	printf("Inicio do teste de insercao.\n");
	printf("Serão INSERIDOS inteiros até que a pilha fique cheia\n");
	while (pushPilha(pilha, i) != -1)
		printf("Inseriu: %d\n", i++);
	printf("Pilha cheia. \n Fim do teste de insercao.\n========================\n\n");
}

// Teste de remocao
void testeRemoverPilha(PDado pilha){
   int valor;
	printf("Inicio do teste de remocao.\n");
	printf("Serão REMOVIDOS inteiros até que a pilha fique vazia\n");
	while (popPilha(pilha, &valor) != -1)
		printf("Removeu: %d\n", valor);
	printf("Pilha vazia. \n Fim do teste de remocao.\n========================\n\n");
}

void testeLeituraArquivoPilha(PDado pilha){
  readPilha("inteiro.txt", pilha);
}



