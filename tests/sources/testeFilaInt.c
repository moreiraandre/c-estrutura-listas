#include <stdio.h>
#include "../../lists/headers/FilaInt.h"
// Teste de insercao
void testeInserirFila(PDado fila){
   int i=0;
	printf("Inicio do teste de insercao.\n");
	printf("Serão INSERIDOS inteiros até que a Fila fique cheia\n");
	while (pushFila(fila, i) != -1)
		printf("Inseriu: %d\n", i++);
	printf("Fila cheia. \n Fim do teste de insercao.\n========================\n\n");
}

// Teste de remocao
void testeRemoverFila(PDado fila){
   int valor;
	printf("Inicio do teste de remocao.\n");
	printf("Serão REMOVIDOS inteiros até que a Fila fique vazia\n");
	while (popFila(fila, &valor) != -1)
		printf("Removeu: %d\n", valor);
	printf("Fila vazia. \n Fim do teste de remocao.\n========================\n\n");
}

void testeLeituraArquivoFila(PDado fila){
  readFila("inteiro.txt", fila);
}



